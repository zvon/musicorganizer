﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace MusicOrganizer
{
    public class Configuration
    {
        private List<MusicProperties> filenameSequence = new List<MusicProperties>();
        private List<string> customFields = new List<string>();
        private List<List<MusicProperties>> directorySequences = new List<List<MusicProperties>>();
        private Dictionary<int, bool> directoryDuplicates = new Dictionary<int, bool>();
        private static readonly MusicProperties[] validDirEntries = { MusicProperties.Album, MusicProperties.Artist, MusicProperties.Genre, MusicProperties.Year };

        private int duplicateIntValue(MusicProperties mp)
        {
            switch (mp)
            {
                case MusicProperties.Album:
                    return 1;
                case MusicProperties.Artist:
                    return 2;
                case MusicProperties.Genre:
                    return 3;
                case MusicProperties.Year:
                    return 4;
            }
            return 0;
        }

        private int hashDuplicate(MusicProperties prop1, MusicProperties prop2, MusicProperties prop3, MusicProperties prop4)
        {
            return duplicateIntValue(prop1) * 1000 + duplicateIntValue(prop2) * 100 + duplicateIntValue(prop3) * 10 + duplicateIntValue(prop4);
        }

        private int hashDuplicate(MusicProperties[] input)
        {
            return hashDuplicate(input[0], input[1], input[2], input[3]);
        }

        private int hashDuplicate(List<MusicProperties> input)
        {
            return hashDuplicate(input[0], input[1], input[2], input[3]);
        }

        // check if `line` is a comment
        private bool isComment(string line)
        {
            if (line.Length == 0)
                return true;
            int i = 0;
            while (Char.IsWhiteSpace(line[i]))
                i++;
            if (line[i] == '#')
                return true;
            return false;
        }

        // if `custom` isn't empty, add it to `filenameSequence`
        // `custom` represents part of filename pattern that isn't
        // to be replaced with metadata
        private void checkCustomPattern(ref string custom)
        {
            if (custom.Length != 0)
            {
                filenameSequence.Add(MusicProperties.Custom);
                customFields.Add(custom);
                custom = "";
            }
        }

        // convert `line` to `filenameSequence`
        private void compileFilenamePattern(string line)
        {
            line = line.Trim();
            string custom = "";
            int curIndex = 0;
            while (curIndex < line.Length)
            {
                int nextIndex = line.IndexOf('%', curIndex);
                if (nextIndex == -1)
                    break;
                custom += line.Substring(curIndex, nextIndex - curIndex);
                curIndex = nextIndex;
                // relies on lazy evaluation, otherwise possible index error
                if (curIndex > 0 && line[curIndex - 1] == '\\')
                {
                    // if we find '\%', it means user wants '%' in custom field
                    custom += '%';
                    curIndex += 1;
                }
                else if (line.IndexOf("album", curIndex) == curIndex + 1)
                {
                    // move i to the last char of 'album'
                    curIndex += 6;
                    // we always check for `custom` before adding
                    // segments to `filenameSequence`
                    checkCustomPattern(ref custom);
                    filenameSequence.Add(MusicProperties.Album);
                }
                else if (line.IndexOf("artist", curIndex) == curIndex + 1)
                {
                    // move i to the last char of 'artist'
                    curIndex += 7;
                    checkCustomPattern(ref custom);
                    filenameSequence.Add(MusicProperties.Artist);
                }
                else if (line.IndexOf("filename", curIndex) == curIndex + 1)
                {
                    // move i to the last char of 'filename'
                    curIndex += 9;
                    checkCustomPattern(ref custom);
                    filenameSequence.Add(MusicProperties.Name);
                }
                else if (line.IndexOf("order", curIndex) == curIndex + 1)
                {
                    // move i to the last char of 'order'
                    curIndex += 6;
                    checkCustomPattern(ref custom);
                    filenameSequence.Add(MusicProperties.Order);
                }
                else if (line.IndexOf("title", curIndex) == curIndex + 1)
                {
                    // move i to the last char of 'title'
                    curIndex += 6;
                    checkCustomPattern(ref custom);
                    filenameSequence.Add(MusicProperties.Title);
                }
                else if (line.IndexOf("year", curIndex) == curIndex + 1)
                {
                    // move i to the last char of 'year'
                    curIndex += 5;
                    checkCustomPattern(ref custom);
                    filenameSequence.Add(MusicProperties.Year);
                }
                else
                {
                    // output % if no escape sequence was found
                    custom += '%';
                    curIndex += 1;
                }
            }

            if (curIndex < line.Length)
                custom += line.Substring(curIndex);
            checkCustomPattern(ref custom);
        }

        // add pattern according to which we should organize the files
        private void addPathPattern(string line)
        {
            var dirStructure = new List<MusicProperties>();
            var segments = line.Split(new string[] { "->" }, StringSplitOptions.RemoveEmptyEntries);

            foreach (var segment in segments)
            {
                switch (segment.Trim().ToLower())
                {
                    case "album":
                        if (dirStructure.Contains(MusicProperties.Album))
                        {
                            Console.WriteLine("Error: You can't specify 'Album' more than once!");
                            return;
                        }
                        dirStructure.Add(MusicProperties.Album);
                        break;
                    case "artist":
                        if (dirStructure.Contains(MusicProperties.Artist))
                        {
                            Console.WriteLine("Error: You can't specify 'Artist' more than once!");
                            return;
                        }
                        dirStructure.Add(MusicProperties.Artist);
                        break;
                    case "genre":
                        if (dirStructure.Contains(MusicProperties.Genre))
                        {
                            Console.WriteLine("Error: You can't specify 'Genre' more than once!");
                            return;
                        }
                        dirStructure.Add(MusicProperties.Genre);
                        break;
                    case "year":
                        if (dirStructure.Contains(MusicProperties.Year))
                        {
                            Console.WriteLine("Error: You can't specify 'Year' more than once!");
                            return;
                        }
                        dirStructure.Add(MusicProperties.Year);
                        break;
                    default:
                        Console.WriteLine("Error: '{0}' is not a valid directory property!", segment);
                        return;
                }
            }
            // ignore duplicate structures
            if (directorySequences.Contains(dirStructure))
                return;
            // Check wheter the dir structure colides with another (e.g. Genre->Artist colides with Genre->Album because they both start with Genre)
            MusicProperties[] temp = { MusicProperties.Custom, MusicProperties.Custom, MusicProperties.Custom, MusicProperties.Custom };
            MusicProperties[] backup = { MusicProperties.Custom, MusicProperties.Custom, MusicProperties.Custom, MusicProperties.Custom };
            for (int i = 0; i < dirStructure.Count; i++)
            {
                temp[i] = dirStructure[i];
                var tempHash = hashDuplicate(temp);
                // we only care about new entries
                if (!directoryDuplicates.ContainsKey(tempHash))
                {
                    // false = might colide in the future
                    directoryDuplicates.Add(tempHash, false);
                    foreach(var mp in validDirEntries)
                    {
                        if(mp != dirStructure[i])
                        {
                            temp[i] = mp;
                            var hash = hashDuplicate(temp);
                            // check whether there exists another directory structure with same `temp[0..i-1]` but different `temp[i]`
                            if(directoryDuplicates.ContainsKey(hash))
                            {
                                var backupHash = hashDuplicate(backup);
                                if (directoryDuplicates.ContainsKey(backupHash))
                                {
                                    directoryDuplicates[backupHash] = true;
                                } else
                                {
                                    // this is needed for root segments
                                    directoryDuplicates.Add(backupHash, true);
                                }
                                break;
                            }
                        }
                    }
                    temp[i] = dirStructure[i];
                }
                backup[i] = dirStructure[i];
            }
            directorySequences.Add(dirStructure);
        }

        public bool validConfig()
        {
            // filenameSequence is the only required part of configuration
            return filenameSequence.Count != 0;
        }

        public void addFilenameConfig(string filenameConfig)
        {
            compileFilenamePattern(filenameConfig);
        }

        public void addDirectoryConfig(string directoryConfig)
        {
            addPathPattern(directoryConfig);
        }

        public void parseConfigFile(string configFile)
        {
            string[] lines = File.ReadAllLines(configFile);
            int i = 0;
            while (isComment(lines[i]))
                i++;
            // first non-coment is file pattern
            compileFilenamePattern(lines[i]);
            i++;
            // all other non-coments are directory structures
            for (; i < lines.Length; i++)
            {
                if (!isComment(lines[i]))
                    addPathPattern(lines[i]);
            }
        }

        // given file's music properties create filename according to `filenameSequence`
        public string compileFileName(Dictionary<MusicProperties, string> fileProperties)
        {
            string ret = "";
            int customIndex = 0;
            foreach (MusicProperties mp in filenameSequence)
            {
                if (mp == MusicProperties.Custom)
                {
                    ret += customFields[customIndex];
                    customIndex++;
                }
                else
                {
                    ret += fileProperties[mp];
                }
            }
            ret += fileProperties[MusicProperties.Extension];
            return ret;
        }

        // create paths in which the file should appear
        public List<string> compileDirs(Dictionary<MusicProperties, string> fileProperties)
        {
            List<string> ret = new List<string>();
            foreach (var sequence in directorySequences)
            {
                string cur = "";
                MusicProperties[] temp = { MusicProperties.Custom, MusicProperties.Custom, MusicProperties.Custom, MusicProperties.Custom };
                for (int i = 0; i < sequence.Count; i++)
                {
                    var mp = sequence[i];
                    var tempHash = hashDuplicate(temp);
                    if (directoryDuplicates.ContainsKey(tempHash) && directoryDuplicates[tempHash])
                        cur += mp + @"\";
                    cur += fileProperties[mp].TrimEnd('.') + @"\";
                    temp[i] = mp;
                }
                ret.Add(cur.Remove(cur.Length - 1));
            }
            return ret;
        }
    }
}

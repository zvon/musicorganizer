﻿namespace MusicOrganizer
{
    public enum MusicProperties
    {
        Album,
        Artist,
        Directory,
        Extension,
        Genre,
        Kind,
        Name,
        Order,
        Path,
        Title,
        Type,
        Year,
        Success,
        Custom,
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace MusicOrganizer
{
    class Program
    {
        static void printHelp(string program)
        {
            Console.WriteLine("Usage:");
            Console.WriteLine("  {0} [options] [path]", program);
            Console.WriteLine("");
            Console.WriteLine("  -h, --help             show this help");
            Console.WriteLine("");
            Console.WriteLine("path needs to be a directory");
            Console.WriteLine("");
            Console.WriteLine("OPTIONS");
            Console.WriteLine("  -p, --pattern          pattern according to which all files will be renamed");
            Console.WriteLine("                           possible escape sequences in the file pattern:");
            Console.WriteLine("                             %album - Album name");
            Console.WriteLine("                             %artist - Artist's name");
            Console.WriteLine("                             %filename - Original filename");
            Console.WriteLine("                             %order - Song's order in the album");
            Console.WriteLine("                             %title - Song's title");
            Console.WriteLine("                             %year - Year when the song / album came out");
            Console.WriteLine("  -s, --structure        directory structer you wish to create (you can provide multiple structures)");
            Console.WriteLine("                         the structure must be provided as follows: 'segment->segment->...'");
            Console.WriteLine("                           possible segments:");
            Console.WriteLine("                             Album");
            Console.WriteLine("                             Artist");
            Console.WriteLine("                             Genre");
            Console.WriteLine("                             Year");
            Console.WriteLine("  -m, --musicdir         path to music dir (doesn't need to be specified, you can just type the directory as last argument without '-m')");
            Console.WriteLine("  -c, --config           path to configuration file");

        }

        static void parseCommnadLine(string[] args, ref string configFile, ref string directory, ref List<string> dirStructures, ref string namePattern, ref bool help)
        {
            int argCount = args.Count();
            for(int i = 0; i < argCount; i++)
            {
                switch(args[i])
                {
                    case "--help":
                    case "-h":
                        help = true;
                        printHelp(System.AppDomain.CurrentDomain.FriendlyName);
                        return;
                    case "--pattern":
                    case "-p":
                        namePattern = args[i + 1];
                        i++;
                        break;
                    case "--structure":
                    case "-s":
                        dirStructures.Add(args[i + 1]);
                        i++;
                        break;
                    case "--musicdir":
                    case "-m":
                        directory = args[i + 1];
                        i++;
                        break;
                    case "--config":
                    case "-c":
                        configFile = args[i + 1];
                        i++;
                        break;
                    default:
                        directory = args[i];
                        break;
                }
            }
        }

        static int Main(string[] args)
        {
            string configFile = "";
            string musicDir = "";
            List<string> dirStructures = new List<string>();
            string namePattern = "";
            bool help = false;

            parseCommnadLine(args, ref configFile, ref musicDir, ref dirStructures, ref namePattern, ref help);
            Configuration cfg = new Configuration();
            if(configFile.Length != 0)
                cfg.parseConfigFile(configFile);
            if (namePattern.Length != 0)
                cfg.addFilenameConfig(namePattern);
            foreach (var x in dirStructures)
                cfg.addDirectoryConfig(x);

            if(!cfg.validConfig() || musicDir.Length == 0)
            {
                System.Console.WriteLine("You didn't provide enough parameters!");
                return 1;
            }

            string backupDir = musicDir + "Backup";
            while (File.Exists(backupDir) || Directory.Exists(backupDir))
            {
                backupDir += "Backup";
            }
            Functions.backupOriginals(musicDir, backupDir);

            Console.Write("Reading music files' information: ");
            Task printProgress = new Task(() =>
            {
                while (Functions.getProgress() < 99.99)
                {
                    Console.Write("{0,6:0.00}%", Functions.getProgress());
                    Console.SetCursorPosition(Console.CursorLeft - 7, Console.CursorTop);
                    Thread.Sleep(100);
                }
            });
            printProgress.Start();
            var musicFiles = Functions.getMusicFilesProperties(backupDir);
            printProgress.Wait();

            Console.WriteLine("Creating new folder structure");
            Functions.moveFiles(musicFiles, cfg, musicDir, backupDir);

            if (!Functions.removeEmptyDirs(backupDir))
                Console.WriteLine("Some files haven't been re-organized, you can find them in directory {0}", backupDir);
            Console.WriteLine("Finished");
            return 0;
        }
    }
}

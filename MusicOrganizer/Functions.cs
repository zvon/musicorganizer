﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;

namespace MusicOrganizer
{
    public class Functions
    {
        private readonly static int maxThreads = 1022;
        private readonly static object propertyLock = new object();
        private static (HashSet<Thread> threads, int availableThreadCount, double percentageAddition, bool finished) threadHandlerData =
            (threads: null, availableThreadCount: maxThreads, percentageAddition: 0.0, finished: false);
        private static AutoResetEvent threadEvent1 = new AutoResetEvent(false);
        private static AutoResetEvent threadEvent2 = new AutoResetEvent(false);
        private static double progress = 0;

        // replace characters illegal in Windows file names
        static private string replaceIllegalChars(string input)
        {
            input = input.Replace("?", "");
            input = input.Replace("\"", "");
            input = input.Replace("\\", "");
            input = input.Replace("/", " ");
            input = input.Replace("*", "");
            input = input.Replace("|", " ");
            input = input.Replace("<", "＜");
            input = input.Replace(">", "＞");
            input = input.Replace(":", " - ");
            while (input.Contains("  "))
                input = input.Replace("  ", " ");
            return input;
        }

        // get mapping from number to music file property in a given directory
        private static Dictionary<MusicProperties, int> getPropertyMapping(Shell32.Folder objFolder)
        {
            var ret = new Dictionary<MusicProperties, int>();
            for (int i = 0; i < short.MaxValue; i++)
            {
                // get name of property corresponding to given property id
                string header = objFolder.GetDetailsOf(null, i);
                if (String.IsNullOrEmpty(header))
                    break;
                switch (header)
                {
                    case "#":
                        ret[MusicProperties.Order] = i;
                        break;
                    case "Album":
                        ret[MusicProperties.Album] = i;
                        break;
                    case "Authors":
                        ret[MusicProperties.Artist] = i;
                        break;
                    case "Genre":
                        ret[MusicProperties.Genre] = i;
                        break;
                    case "Kind":
                        ret[MusicProperties.Kind] = i;
                        break;
                    case "Perceived type":
                        ret[MusicProperties.Type] = i;
                        break;
                    case "Title":
                        ret[MusicProperties.Title] = i;
                        break;
                    case "Year":
                        ret[MusicProperties.Year] = i;
                        break;
                    default:
                        break;
                }
            }
            return ret;
        }

        // get music properties for all files in given folder
        private static List<Dictionary<MusicProperties, string>> getMusicEntries(Shell32.Folder objFolder, Dictionary<MusicProperties, int> propertyMapping, string dir,
            (string genre, string artists, string album, string year) defVals)
        {
            var ret = new List<Dictionary<MusicProperties, string>>();

            foreach (Shell32.FolderItem2 item in objFolder.Items())
            {
                if (objFolder.GetDetailsOf(item, propertyMapping[MusicProperties.Type]) != "Audio" ||
                    objFolder.GetDetailsOf(item, propertyMapping[MusicProperties.Kind]) == "Folder")
                    continue;
                Dictionary<MusicProperties, string> newEntry = new Dictionary<MusicProperties, string>();
                newEntry[MusicProperties.Album] = replaceIllegalChars(objFolder.GetDetailsOf(item, propertyMapping[MusicProperties.Album]));
                newEntry[MusicProperties.Artist] = replaceIllegalChars(objFolder.GetDetailsOf(item, propertyMapping[MusicProperties.Artist]));
                newEntry[MusicProperties.Genre] = replaceIllegalChars(objFolder.GetDetailsOf(item, propertyMapping[MusicProperties.Genre]));
                newEntry[MusicProperties.Order] = replaceIllegalChars(objFolder.GetDetailsOf(item, propertyMapping[MusicProperties.Order]));
                newEntry[MusicProperties.Title] = replaceIllegalChars(objFolder.GetDetailsOf(item, propertyMapping[MusicProperties.Title]));
                newEntry[MusicProperties.Year] = replaceIllegalChars(objFolder.GetDetailsOf(item, propertyMapping[MusicProperties.Year]));
                newEntry[MusicProperties.Directory] = dir;
                newEntry[MusicProperties.Path] = item.Path;

                // if user has 'show file extensions' enabled in explorer, there will be extension in `item.Name`, otherwise
                // it won't be there
                int extensionIndex = item.Name.LastIndexOf('.');
                string extensionName = "";
                if (extensionIndex >= 0)
                    extensionName = item.Name.Substring(extensionIndex);

                int extensionPathIndex = item.Path.LastIndexOf('.');
                string extensionFile = "";
                if (extensionPathIndex >= 0)
                    extensionFile = item.Path.Substring(extensionPathIndex);
                if (extensionName == extensionFile)
                {
                    if (extensionIndex >= 0)
                        newEntry[MusicProperties.Name] = item.Name.Remove(extensionIndex);
                    else
                        newEntry[MusicProperties.Name] = item.Name;
                    newEntry[MusicProperties.Extension] = extensionName;
                }
                else
                {
                    newEntry[MusicProperties.Name] = item.Name;
                    newEntry[MusicProperties.Extension] = extensionFile;
                }
                ret.Add(newEntry);

                // initialize default values according to first item that has these values
                if (defVals.genre == "" && newEntry[MusicProperties.Genre] != "")
                    defVals.genre = newEntry[MusicProperties.Genre];
                if (defVals.artists == "" && newEntry[MusicProperties.Artist] != "")
                    defVals.artists = newEntry[MusicProperties.Artist];
                if (defVals.album == "" && newEntry[MusicProperties.Album] != "")
                    defVals.album = newEntry[MusicProperties.Album];
                if (defVals.year == "" && newEntry[MusicProperties.Year] != "")
                    defVals.year = newEntry[MusicProperties.Year];
            }

            return ret;
        }

        // inspiration for this code from - https://stackoverflow.com/questions/31403956/exception-when-using-shell32-to-get-file-extended-properties
        // get music properties for all files in given folder
        static private void getMusicFilesPropertiesInternal(object param)
        {
            var inputParam = (Tuple<string, List<Dictionary<MusicProperties, string>>>)param;
            var dir = inputParam.Item1;
            var output = inputParam.Item2;

            Shell32.Shell shell = new Shell32.Shell();
            Shell32.Folder objFolder = shell.NameSpace(dir);

            var propertyMapping = getPropertyMapping(objFolder);

            var defaultValues = (genre: "", artists: "", album: "", year: "");
            var localEntries = getMusicEntries(objFolder, propertyMapping, dir, defaultValues);

            if (defaultValues.genre == "")
                defaultValues.genre = "Unknown";
            if (defaultValues.artists == "")
                defaultValues.artists = "Unknown";
            if (defaultValues.album == "")
                defaultValues.album = "Unknown";
            if (defaultValues.year == "")
                defaultValues.year = "Unknown";

            // fill empty values with default entries
            foreach (var entry in localEntries)
            {
                if (entry[MusicProperties.Genre] == "")
                    entry[MusicProperties.Genre] = defaultValues.genre;
                if (entry[MusicProperties.Artist] == "")
                    entry[MusicProperties.Artist] = defaultValues.artists;
                if (entry[MusicProperties.Album] == "")
                    entry[MusicProperties.Album] = defaultValues.album;
                if (entry[MusicProperties.Year] == "")
                    entry[MusicProperties.Year] = defaultValues.year;
                if (entry[MusicProperties.Title] == "")
                    entry[MusicProperties.Title] = entry[MusicProperties.Name];
            }

            lock (propertyLock)
            {
                output.AddRange(localEntries);
            }
        }

        // control threads and update information about progress
        static private void threadHandler(object param)
        {
            // wait for threads to be prepared
            threadEvent2.WaitOne();
            threadEvent2.Reset();
            ref int availThreads = ref threadHandlerData.availableThreadCount;
            ref HashSet<Thread> threads = ref threadHandlerData.threads;
            double addition = threadHandlerData.percentageAddition;
            progress = 0.0;
            List<Thread> toRemove = new List<Thread>();
            while (threads.Count != 0)
            {
                foreach (Thread t in threads)
                {
                    // wait 100 ms, test if thread has finished
                    if (t.Join(100))
                    {
                        progress += addition;
                        toRemove.Add(t);
                    }
                }

                if (toRemove.Count != 0)
                {
                    foreach (Thread t in toRemove)
                    {
                        threads.Remove(t);
                        availThreads += 1;
                    }
                    toRemove.Clear();
                    // only signal if more threads need to be spawned
                    if (!threadHandlerData.finished)
                    {
                        // let the main thread know that more threads can be spawned
                        threadEvent1.Set();
                        // wait until the threads are spawned
                        threadEvent2.WaitOne();
                        threadEvent2.Reset();
                    }
                }
            }
        }

        // remove empty dirs, return true if `dir` has been deleted
        static public bool removeEmptyDirs(string dir)
        {
            var dirs = Filesystem.getDirs(dir);
            // start from deepest dir
            dirs.Reverse();
            foreach (var curDir in dirs)
            {
                // If directore's empty, delete it
                if (Directory.GetFiles(curDir).Length == 0 && Directory.GetDirectories(curDir).Length == 0)
                    Directory.Delete(curDir);
            }
            if (Directory.GetFiles(dir).Length == 0 && Directory.GetDirectories(dir).Length == 0)
            {
                Directory.Delete(dir);
                return true;
            }
            return false;
        }

        static public double getProgress()
        {
            return progress;
        }

        static public List<Dictionary<MusicProperties, string>> getMusicFilesProperties(string dir)
        {
            var ret = new List<Dictionary<MusicProperties, string>>();
            var dirs = Filesystem.getDirs(dir);
            dirs.Add(dir);
            HashSet<Thread> threads = new HashSet<Thread>();

            // how much each processed dir adds to overall percentage of processed files
            // (not very precise, but good enough for eye-balling)
            double addition = 100.0 / (double)dirs.Count;
            threadHandlerData.percentageAddition = addition;
            threadHandlerData.threads = threads;
            ref int availableThreads = ref threadHandlerData.availableThreadCount;

            Thread threadHandlerThread = new Thread(new ParameterizedThreadStart(threadHandler));
            threadHandlerThread.Start(null);

            // we allocate 1 thread per directory
            for (int i = 0; i < dirs.Count; i++)
            {
                // We have to use STA thread because of Shell32
                Thread propertiesThread = new Thread(new ParameterizedThreadStart(getMusicFilesPropertiesInternal));
                propertiesThread.SetApartmentState(ApartmentState.STA);
                propertiesThread.Start(new Tuple<string, List<Dictionary<MusicProperties, string>>>(dirs[i], ret));
                threads.Add(propertiesThread);
                availableThreads -= 1;
                if (availableThreads == 0)
                {
                    // let ThreadHandler know that max amount of threads are running
                    threadEvent2.Set();
                    // wait for ThreadHandler to join some threads before continuing
                    threadEvent1.WaitOne();
                    threadEvent1.Reset();
                }
            }
            // no more threads need to be spawned
            threadHandlerData.finished = true;
            // let ThreadHandler know it can join all threads
            threadEvent2.Set();
            threadHandlerThread.Join();

            return ret;
        }

        // move original directory to `backupDir`
        static public void backupOriginals(string musicDir, string backupDir)
        {
            Directory.Move(musicDir, backupDir);
            Directory.CreateDirectory(musicDir);
        }

        static private void moveFilesInternal(string outputDir, string backupDir, string filename, Dictionary<MusicProperties, string> properties )
        {
            if (!Directory.Exists(outputDir))
                Directory.CreateDirectory(outputDir);
            var outputPath = outputDir + @"\" + filename;
            if (Filesystem.createLink(properties[MusicProperties.Path], outputPath))
            {
                if (properties[MusicProperties.Success] != "N")
                    properties[MusicProperties.Success] = "Y";
            }
            else
            {
                properties[MusicProperties.Success] = "N";
                Console.WriteLine("Couldn't move file {0} to {1}, it can be found in the backup directory {2}", properties[MusicProperties.Path], outputPath, backupDir);
            }
        }

        // move files from original directory to new directory structure under their new filename
        static public void moveFiles(List<Dictionary<MusicProperties, string>> musicFiles, Configuration cfg, String musicDir, String backupDir)
        {
            double addition = 100 / musicFiles.Count;
            progress = 0.0;
            foreach (var x in musicFiles)
            {
                var dirs = cfg.compileDirs(x);
                var filename = cfg.compileFileName(x);
                x[MusicProperties.Success] = "?";
                foreach (var dir in dirs)
                {
                    var outputDir = musicDir + @"\" + dir;
                    moveFilesInternal(outputDir, backupDir, filename, x);
                }
                if(dirs.Count == 0)
                {
                    var outputDir = x[MusicProperties.Directory].Replace(backupDir, musicDir);
                    moveFilesInternal(outputDir, backupDir, filename, x);
                }
                if (x[MusicProperties.Success] == "Y")
                    File.Delete(x[MusicProperties.Path]);
                progress += addition;
            }
            progress = 100;
        }
    }
}

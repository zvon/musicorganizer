﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;

namespace MusicOrganizer
{
    public class Filesystem
    {
        // idea from https://stackoverflow.com/questions/3387690/how-to-create-a-hardlink-in-c
        [DllImport("Kernel32.dll", CharSet = CharSet.Unicode)]
        static extern private bool CreateHardLink(string lpFileName, string lpExistingFileName, IntPtr lpSecurityAttributes);

        static public List<string> getDirs(string dir)
        {
            var ret = new List<string>();
            foreach (var f in Directory.GetDirectories(dir, "*", SearchOption.AllDirectories))
            {
                ret.Add(f);
            }
            return ret;
        }

        static public bool createLink(string source, string target)
        {
            return CreateHardLink(target, source, IntPtr.Zero);
        }
    }
}

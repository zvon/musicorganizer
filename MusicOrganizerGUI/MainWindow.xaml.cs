﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.IO;

namespace MusicOrganizerGUI
{
    public partial class MainWindow : Window
    {
        // this holds list of dir-structure properties that user creates in the UI
        private List<List<MusicOrganizer.MusicProperties>> itemMusicProperties = new List<List<MusicOrganizer.MusicProperties>>();

        public MainWindow()
        {
            InitializeComponent();
        }

        private void browseFolder(object sender, RoutedEventArgs e)
        {
            var folderBrowser = new System.Windows.Forms.FolderBrowserDialog();
            folderBrowser.Description = "Choose music directory";
            folderBrowser.ShowNewFolderButton = false;
            var result = folderBrowser.ShowDialog();
            if(result == System.Windows.Forms.DialogResult.OK)
            {
                textboxDir.Text = folderBrowser.SelectedPath;
            }
        }

        private void quit(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        // show window with name pattern help
        private void patternHelpClick(object sender, RoutedEventArgs e)
        {
            PatternHelp patternHelp = new PatternHelp();
            patternHelp.Show();
        }

        // add new item to listbox and add new dir-structure to itemMusicProperties
        private void addDirStruct(object sender, RoutedEventArgs e)
        {
            listboxDirStructure.Items.Add("<Empty>");
            itemMusicProperties.Add(new List<MusicOrganizer.MusicProperties>());
            // automatically select the new item
            listboxDirStructure.SelectedIndex = itemMusicProperties.Count - 1;
        }

        // only enable buttons which make sense based on the selected item in listboxDirStructure
        private void updateEnabledButtons(int index)
        {
            if (index == -1)
            {
                buttonAlbum.IsEnabled = false;
                buttonArtist.IsEnabled = false;
                buttonGenre.IsEnabled = false;
                buttonYear.IsEnabled = false;
                buttonRmLast.IsEnabled = false;
                return;
            }

            var properties = itemMusicProperties[index];
            if (properties.Contains(MusicOrganizer.MusicProperties.Album))
                buttonAlbum.IsEnabled = false;
            else
                buttonAlbum.IsEnabled = true;

            if (properties.Contains(MusicOrganizer.MusicProperties.Artist))
                buttonArtist.IsEnabled = false;
            else
                buttonArtist.IsEnabled = true;

            if (properties.Contains(MusicOrganizer.MusicProperties.Genre))
                buttonGenre.IsEnabled = false;
            else
                buttonGenre.IsEnabled = true;

            if (properties.Contains(MusicOrganizer.MusicProperties.Year))
                buttonYear.IsEnabled = false;
            else
                buttonYear.IsEnabled = true;

            if (properties.Count == 0)
                buttonRmLast.IsEnabled = false;
            else
                buttonRmLast.IsEnabled = true;
        }

        // remove item from listbox and itemMusicProperties
        private void deleteDirStruct(object sender, RoutedEventArgs e)
        {
            int index = listboxDirStructure.SelectedIndex;
            if ( index < 0)
                return;
            itemMusicProperties.RemoveAt(index);
            listboxDirStructure.Items.RemoveAt(index);
            // after deletion no item is selected
            updateEnabledButtons(-1);
        }

        private void dirItemSelectionChange(object sender, SelectionChangedEventArgs e)
        {
            updateEnabledButtons(listboxDirStructure.SelectedIndex);
        }

        // update item's text based on selected music properties
        private void changeItemText(int index)
        {
            var properties = itemMusicProperties[index];
            if(properties.Count == 0)
            {
                listboxDirStructure.Items[index] = "<Empty>";
                listboxDirStructure.SelectedIndex = index;
                return;
            }
            String itemText = "";
            for(int i = 0; i < properties.Count - 1; i++)
            {
                itemText += properties[i];
                itemText += "->";
            }
            itemText += properties.Last();

            listboxDirStructure.Items[index] = itemText;
            // changing item's text de-selects it, so we need to select it again
            listboxDirStructure.SelectedIndex = index;
        }

        private void itemModifyMusicProperty(object sender, RoutedEventArgs e)
        {
            int index = listboxDirStructure.SelectedIndex;
            if (index < 0)
                return;
            if(sender == buttonAlbum)
                itemMusicProperties[index].Add(MusicOrganizer.MusicProperties.Album);
            else if(sender == buttonArtist)
                itemMusicProperties[index].Add(MusicOrganizer.MusicProperties.Artist);
            else if(sender == buttonGenre)
                itemMusicProperties[index].Add(MusicOrganizer.MusicProperties.Genre);
            else if(sender == buttonYear)
                itemMusicProperties[index].Add(MusicOrganizer.MusicProperties.Year);
            else if(sender == buttonRmLast)
            {
                var properties = itemMusicProperties[index];
                if (properties.Count == 0)
                    return;
                properties.RemoveAt(properties.Count - 1);
            }
            changeItemText(index);
            updateEnabledButtons(index);
        }

        void showProgress(object obj)
        {
            ProgressWindow progressWindow = null;
            this.Dispatcher.Invoke(() =>
            {
                var progresswnd = new ProgressWindow();
                progresswnd.Show();
                progressWindow = progresswnd;
                progressWindow.label.Content = (String)obj;
            });
            while (MusicOrganizer.Functions.getProgress() < 99.99)
            {
                this.Dispatcher.Invoke(() =>
                {
                    progressWindow.progressBar.Value = MusicOrganizer.Functions.getProgress();
                });
                Thread.Sleep(100);
            }
            this.Dispatcher.Invoke(() =>
            {
                progressWindow.Close();
            });
        }

        void processAndMoveFiles(object obj)
        {
            var input = (Tuple<string, string>)obj;
            string musicDir = input.Item1;
            string filenamePattern = input.Item2;

            string backupDir = musicDir + "Backup";
            while (File.Exists(backupDir) || Directory.Exists(backupDir))
            {
                backupDir += "Backup";
            }
            MusicOrganizer.Functions.backupOriginals(musicDir, backupDir);

            MusicOrganizer.Configuration cfg = new MusicOrganizer.Configuration();
            cfg.addFilenameConfig(filenamePattern);

            foreach(var x in listboxDirStructure.Items)
            {
                cfg.addDirectoryConfig((string)x);
            }

            Thread thread = new Thread(new ParameterizedThreadStart(showProgress));
            thread.SetApartmentState(ApartmentState.STA);
            thread.Start("Getting information about files");
            var musicProperties = MusicOrganizer.Functions.getMusicFilesProperties(backupDir);
            thread.Join();

            thread = new Thread(new ParameterizedThreadStart(showProgress));
            thread.SetApartmentState(ApartmentState.STA);
            thread.Start("Moving files");
            MusicOrganizer.Functions.moveFiles(musicProperties, cfg, musicDir, backupDir);
            if (!MusicOrganizer.Functions.removeEmptyDirs(backupDir))
            {
                var message = "Some files haven't been re-organized, you can find them in directory " + backupDir;
                MessageBox.Show(message, "Unmoved files", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void process(object sender, RoutedEventArgs e)
        {
            // starting in a different thread, so GUI doesn't freeze
            Thread thread = new Thread(new ParameterizedThreadStart(processAndMoveFiles));
            thread.Start(new Tuple<string, string>(textboxDir.Text, textboxName.Text));
        }
    }
}

# MusicOrganizer

### This is my project for PV178 (Úvod do vývoje v C#/.NET)

This program organizes music files in given folder.

The program uses metadata from files to:
  - rename the files
  - create a folder structure and move files to appropriate folders (optional)

You can use the following metadata for renaming files:
  - Album
  - Artist
  - Original filename
  - Song's order in its album
  - Title of the song
  - Year the album was released

You can use the following metadata to create directory structure:
  - Album
  - Artist
  - Genre
  - Year the album was released

You can specify multiple directory structures, files in these structures will
be hard-links, not actual files, so no extra space will be used if you specify
multiple structures.

## CLI options:

```
-p, --pattern    pattern according to which all files will be renamed
-s, --structure  directory structer you wish to create (you can provide multiple structures) the structure must be provided as follows: 'segment->segment->...'
-m, --musicdir  path to music dir (doesn't need to be specified, you can just type the directory as last argument without '-m')
-c, --config    path to configuration file
```

## Configuration file structure

First line which isn't a comment represents the file name pattern, all other non-comment lines represent directory structures
